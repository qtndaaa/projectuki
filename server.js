var express = require('express');
var app = express();
var mongojs = require('mongojs');
var db = mongojs('employees', ['emp']);
var bodyParser = require('body-parser');

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());

app.get('/emp', function (req, res) {
  console.log('I received a GET request');

  db.emp.find(function (err, docs) {
    console.log(docs);
    res.json(docs);
  });
});

app.post('/emp', function (req, res) {
  console.log(req.body);
  db.emp.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/emp/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.emp.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/emp/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.emp.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/emp/:id', function (req, res) {
  var id = req.params.id;
  // console.log(req.body.name);
  db.emp.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {code_emp: req.body.code_emp, name: req.body.name, department: req.body.department}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.listen(3001);
console.log("Server running on port 3000");